<?php

namespace Drupal\cache_metrics;

use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Wraps a cache factory to register all calls to the cache system.
 *
 * Inspired by webprofiler.
 */
class CacheFactoryWrapper implements CacheFactoryInterface {

  /**
   * All wrapped cache backends.
   *
   * @var \Drupal\cache_metrics\CacheBackendWrapper[]
   */
  protected $cacheBackends = [];

  public function __construct(
    protected CacheFactoryInterface $cacheFactory,
    protected AccountProxyInterface $currentUser,
    protected RequestStack $requestStack,
    protected array $blacklist,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function get($bin) {
    if (!$this->isEnabled($bin)) {
      // If disabled, return an unwrapped backend.
      return $this->cacheFactory->get($bin);
    }

    if (!isset($this->cacheBackends[$bin])) {
      $cache_backend = $this->cacheFactory->get($bin);
      $this->cacheBackends[$bin] = new CacheBackendWrapper($cache_backend, $bin, $this->currentUser, $this->requestStack);
    }
    return $this->cacheBackends[$bin];
  }

  /**
   * Use services.yml parameter to disable logging for bins.
   *
   * @param string $bin
   *   String value.
   *
   * @return bool
   *   return the boolean value.
   */
  public function isEnabled($bin) {
    return !in_array('*', $this->blacklist) && !in_array($bin, $this->blacklist) && function_exists('newrelic_record_custom_event');
  }

}
